import javax.crypto.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import sun.misc.*;
public class DesDemo{
	static final String UNICODE_FORMAT = "UTF8";
	static final String ENCRYPTION_SCHEME = "DES";
	KeySpec keySpec;
	SecretKeyFactory secretKeyFactory;
	Cipher cipher;
	byte[] byteKey;
	SecretKey secretKey;
	DesDemo(String key){
		try{
			byteKey=key.getBytes(UNICODE_FORMAT);
			keySpec=new DESKeySpec(byteKey);
			secretKeyFactory=SecretKeyFactory.getInstance(ENCRYPTION_SCHEME);
			cipher=Cipher.getInstance(ENCRYPTION_SCHEME);
			secretKey=secretKeyFactory.generateSecret(keySpec);
		}catch(Exception e){
			System.out.println("Exception in constructor"+e);
		}
	}
	String encrypt(String plainText){
		String encryptedString="";
		System.out.println("Inside function "+plainText);
		try{
			System.out.println("Inside try");
			cipher.init(Cipher.ENCRYPT_MODE,secretKey);
			byte[] text=plainText.getBytes(UNICODE_FORMAT);
			byte[] encrypted=cipher.doFinal(text);
			BASE64Encoder b64=new BASE64Encoder();
			// System.out.println("inside encryption : "+encrypted+"\n");
			encryptedString=b64.encode(encrypted);
		}catch(Exception e){
			System.out.println("Exception in encrypt "+e);
		}
		return encryptedString;
	}
	String decrypt(String encryptedText){
		String plainText="";
		try{
			cipher.init(Cipher.DECRYPT_MODE,secretKey);
			BASE64Decoder b64=new BASE64Decoder();
			byte[] encrypted=b64.decodeBuffer(encryptedText);
			byte[] plain=cipher.doFinal(encrypted);
			plainText=new String(plain);
		}catch(Exception e){
			System.out.println("Exception in decrypt "+e);
		}
		return plainText;
	}
	public static void main(String[] args) {
		DesDemo des=new DesDemo("des algo");
		String text="jash vijay doshi";
		String en=des.encrypt(text);
		String de=des.decrypt(en);
		System.out.println("Plain text : "+text);
		System.out.println("Encrypted text : "+en);
		System.out.println("Decrypted text : "+de);
	}
}







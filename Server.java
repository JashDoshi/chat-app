
import java.io.IOException;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.*;

public class Server{
	public static void main(String[] args) {
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                
                    HashMap<String,Socket> hm=new HashMap<>();
                    String names="";
                    String name;
                    try{
                            ServerSocket server=new ServerSocket(5000);
                            while(true){
                                    Socket socket=server.accept();

                                    System.out.println("inside server");
                                    BufferedReader input=new BufferedReader(new InputStreamReader(socket.getInputStream()));
                                    name=input.readLine();
                                    hm.put(name,socket);
                                    
                                    System.out.println(name);
                                    new ServerEchoer(socket,hm).start();

                            }
                    }catch(IOException e){
                            System.out.println("Issue : "+e.getMessage());
                    }
                }
            });
        }
       
}